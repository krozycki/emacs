;;; web-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (web-mode) "web-mode" "web-mode.el" (21568 50446
;;;;;;  656815 37000))
;;; Generated autoloads from web-mode.el

(autoload 'web-mode "web-mode" "\
Major mode for editing web templates.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("web-mode-pkg.el") (21568 50446 768331
;;;;;;  260000))

;;;***

(provide 'web-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; web-mode-autoloads.el ends here
